import {ApplicationConfig} from '@loopback/core';
import {CustomersDataApplication} from './application';

export {CustomersDataApplication};

export async function main(options: ApplicationConfig = {}) {
  const app = new CustomersDataApplication(options);
  await app.boot();

  // auto-update database at start
  await app.migrateSchema();

  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}
